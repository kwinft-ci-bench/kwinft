/********************************************************************
KWin - the KDE window manager
This file is part of the KDE project.

Copyright (C) 2015 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#include "lib/app.h"

#include "base/wayland/server.h"
#include "base/x11/atoms.h"
#include "input/cursor.h"
#include "render/effects.h"
#include "win/active_window.h"
#include "win/input.h"
#include "win/move.h"
#include "win/placement.h"
#include "win/space.h"
#include "win/wayland/window.h"
#include "win/x11/window.h"

#include <Wrapland/Client/compositor.h>
#include <Wrapland/Client/connection_thread.h>
#include <Wrapland/Client/plasmashell.h>
#include <Wrapland/Client/pointer.h>
#include <Wrapland/Client/seat.h>
#include <Wrapland/Client/surface.h>
#include <Wrapland/Client/xdg_shell.h>

#include <linux/input.h>
#include <xcb/xcb_icccm.h>

Q_DECLARE_METATYPE(KWin::win::quicktiles)

namespace KWin
{

class MoveResizeWindowTest : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void initTestCase();
    void init();
    void cleanup();
    void testMove();
    void testResize();
    void testPackTo_data();
    void testPackTo();
    void testPackAgainstClient_data();
    void testPackAgainstClient();
    void testGrowShrink_data();
    void testGrowShrink();
    void testPointerMoveEnd_data();
    void testPointerMoveEnd();
    void testClientSideMove();
    void testPlasmaShellSurfaceMovable_data();
    void testPlasmaShellSurfaceMovable();
    void testNetMove();
    void testAdjustClientGeometryOfAutohidingX11Panel_data();
    void testAdjustClientGeometryOfAutohidingX11Panel();
    void testAdjustClientGeometryOfAutohidingWaylandPanel_data();
    void testAdjustClientGeometryOfAutohidingWaylandPanel();
    void testDestroyMoveClient();
    void testDestroyResizeClient();
    void testUnmapMoveClient();
    void testUnmapResizeClient();
    void testSetFullScreenWhenMoving();
    void testSetMaximizeWhenMoving();

private:
    Wrapland::Client::ConnectionThread* m_connection = nullptr;
    Wrapland::Client::Compositor* m_compositor = nullptr;
};

Test::space::x11_window* get_x11_window_from_id(uint32_t id)
{
    return Test::get_x11_window(Test::app()->base->space->windows_map.at(id));
}

void MoveResizeWindowTest::initTestCase()
{
    QSignalSpy startup_spy(Test::app(), &WaylandTestApplication::startup_finished);
    QVERIFY(startup_spy.isValid());

    Test::app()->start();
    QVERIFY(startup_spy.wait());
    Test::test_outputs_geometries({{0, 0, 1280, 1024}});
}

void MoveResizeWindowTest::init()
{
    Test::setup_wayland_connection(Test::global_selection::plasma_shell
                                   | Test::global_selection::seat);
    QVERIFY(Test::wait_for_wayland_pointer());
    m_connection = Test::get_client().connection;
    m_compositor = Test::get_client().interfaces.compositor.get();
}

void MoveResizeWindowTest::cleanup()
{
    Test::destroy_wayland_connection();
}

void MoveResizeWindowTest::testMove()
{
    using namespace Wrapland::Client;

    std::unique_ptr<Surface> surface(Test::create_surface());
    QVERIFY(surface);

    std::unique_ptr<XdgShellToplevel> shellSurface(Test::create_xdg_shell_toplevel(surface));
    QVERIFY(shellSurface);

    auto c = Test::render_and_wait_for_shown(surface, QSize(100, 50), Qt::blue);
    QVERIFY(c);
    QCOMPARE(Test::get_wayland_window(Test::app()->base->space->stacking.active), c);
    QCOMPARE(c->geo.frame, QRect(0, 0, 100, 50));

    QSignalSpy geometryChangedSpy(c->qobject.get(), &win::window_qobject::frame_geometry_changed);
    QVERIFY(geometryChangedSpy.isValid());
    QSignalSpy startMoveResizedSpy(c->qobject.get(),
                                   &win::window_qobject::clientStartUserMovedResized);
    QVERIFY(startMoveResizedSpy.isValid());
    QSignalSpy moveResizedChangedSpy(c->qobject.get(), &win::window_qobject::moveResizedChanged);
    QVERIFY(moveResizedChangedSpy.isValid());
    QSignalSpy clientStepUserMovedResizedSpy(c->qobject.get(),
                                             &win::window_qobject::clientStepUserMovedResized);
    QVERIFY(clientStepUserMovedResizedSpy.isValid());
    QSignalSpy clientFinishUserMovedResizedSpy(c->qobject.get(),
                                               &win::window_qobject::clientFinishUserMovedResized);
    QVERIFY(clientFinishUserMovedResizedSpy.isValid());

    // effects signal handlers
    QSignalSpy windowStartUserMovedResizedSpy(effects,
                                              &EffectsHandler::windowStartUserMovedResized);
    QVERIFY(windowStartUserMovedResizedSpy.isValid());
    QSignalSpy windowStepUserMovedResizedSpy(effects, &EffectsHandler::windowStepUserMovedResized);
    QVERIFY(windowStepUserMovedResizedSpy.isValid());
    QSignalSpy windowFinishUserMovedResizedSpy(effects,
                                               &EffectsHandler::windowFinishUserMovedResized);
    QVERIFY(windowFinishUserMovedResizedSpy.isValid());

    QVERIFY(!Test::app()->base->space->move_resize_window);
    QCOMPARE(win::is_move(c), false);

    // begin move
    win::active_window_move(*Test::app()->base->space);
    QCOMPARE(Test::get_wayland_window(Test::app()->base->space->move_resize_window), c);
    QCOMPARE(startMoveResizedSpy.count(), 1);
    QCOMPARE(moveResizedChangedSpy.count(), 1);
    QCOMPARE(windowStartUserMovedResizedSpy.count(), 1);
    QCOMPARE(win::is_move(c), true);
    QCOMPARE(c->geo.restore.max, QRect(0, 0, 100, 50));

    // send some key events, not going through input redirection
    auto const cursorPos = Test::cursor()->pos();
    win::key_press_event(c, Qt::Key_Right);
    win::update_move_resize(c, Test::cursor()->pos());
    QCOMPARE(Test::cursor()->pos(), cursorPos + QPoint(8, 0));
    QEXPECT_FAIL("", "First event is ignored", Continue);
    QCOMPARE(clientStepUserMovedResizedSpy.count(), 1);
    clientStepUserMovedResizedSpy.clear();
    windowStepUserMovedResizedSpy.clear();

    win::key_press_event(c, Qt::Key_Right);
    win::update_move_resize(c, Test::cursor()->pos());
    QCOMPARE(Test::cursor()->pos(), cursorPos + QPoint(16, 0));
    QCOMPARE(clientStepUserMovedResizedSpy.count(), 1);
    QCOMPARE(windowStepUserMovedResizedSpy.count(), 1);

    win::key_press_event(c, Qt::Key_Down | Qt::ALT);
    win::update_move_resize(c, Test::cursor()->pos());
    QCOMPARE(clientStepUserMovedResizedSpy.count(), 2);
    QCOMPARE(windowStepUserMovedResizedSpy.count(), 2);
    QCOMPARE(c->geo.frame, QRect(16, 32, 100, 50));
    QCOMPARE(Test::cursor()->pos(), cursorPos + QPoint(16, 32));

    // let's end
    QCOMPARE(clientFinishUserMovedResizedSpy.count(), 0);
    win::key_press_event(c, Qt::Key_Enter);
    QCOMPARE(clientFinishUserMovedResizedSpy.count(), 1);
    QCOMPARE(moveResizedChangedSpy.count(), 2);
    QCOMPARE(windowFinishUserMovedResizedSpy.count(), 1);
    QCOMPARE(c->geo.frame, QRect(16, 32, 100, 50));
    QCOMPARE(win::is_move(c), false);
    QVERIFY(!Test::app()->base->space->move_resize_window);
    surface.reset();
    QVERIFY(Test::wait_for_destroyed(c));
}

void MoveResizeWindowTest::testResize()
{
    // a test case which manually resizes a window
    using namespace Wrapland::Client;

    std::unique_ptr<Surface> surface(Test::create_surface());
    QVERIFY(surface);

    std::unique_ptr<XdgShellToplevel> shellSurface(
        Test::create_xdg_shell_toplevel(surface, Test::CreationSetup::CreateOnly));
    QVERIFY(shellSurface);

    // Wait for the initial configure event.
    QSignalSpy configureRequestedSpy(shellSurface.get(), &XdgShellToplevel::configured);
    QVERIFY(configureRequestedSpy.isValid());
    surface->commit(Surface::CommitFlag::None);
    QVERIFY(configureRequestedSpy.wait());
    QCOMPARE(configureRequestedSpy.count(), 1);

    auto cfgdata = shellSurface->get_configure_data();
    QVERIFY(!cfgdata.states.testFlag(xdg_shell_state::activated));
    QVERIFY(!cfgdata.states.testFlag(xdg_shell_state::resizing));

    // Let's render.
    shellSurface->ackConfigure(configureRequestedSpy.back().front().value<quint32>());
    auto c = Test::render_and_wait_for_shown(surface, QSize(100, 50), Qt::blue);

    // We have to receive a configure event when the client becomes active.
    QVERIFY(configureRequestedSpy.wait());
    QCOMPARE(configureRequestedSpy.count(), 2);

    cfgdata = shellSurface->get_configure_data();
    QVERIFY(cfgdata.states.testFlag(xdg_shell_state::activated));
    QVERIFY(!cfgdata.states.testFlag(xdg_shell_state::resizing));
    QVERIFY(cfgdata.updates.testFlag(xdg_shell_toplevel_configure_change::size));

    QVERIFY(c);
    QCOMPARE(Test::get_wayland_window(Test::app()->base->space->stacking.active), c);
    QCOMPARE(c->geo.frame, QRect(0, 0, 100, 50));
    QSignalSpy geometryChangedSpy(c->qobject.get(), &win::window_qobject::frame_geometry_changed);
    QVERIFY(geometryChangedSpy.isValid());
    QSignalSpy startMoveResizedSpy(c->qobject.get(),
                                   &win::window_qobject::clientStartUserMovedResized);
    QVERIFY(startMoveResizedSpy.isValid());
    QSignalSpy moveResizedChangedSpy(c->qobject.get(), &win::window_qobject::moveResizedChanged);
    QVERIFY(moveResizedChangedSpy.isValid());
    QSignalSpy clientStepUserMovedResizedSpy(c->qobject.get(),
                                             &win::window_qobject::clientStepUserMovedResized);
    QVERIFY(clientStepUserMovedResizedSpy.isValid());
    QSignalSpy clientFinishUserMovedResizedSpy(c->qobject.get(),
                                               &win::window_qobject::clientFinishUserMovedResized);
    QVERIFY(clientFinishUserMovedResizedSpy.isValid());

    // begin resize
    QVERIFY(!Test::app()->base->space->move_resize_window);
    QCOMPARE(win::is_move(c), false);
    QCOMPARE(win::is_resize(c), false);
    win::active_window_resize(*Test::app()->base->space);
    QCOMPARE(Test::get_wayland_window(Test::app()->base->space->move_resize_window), c);
    QCOMPARE(startMoveResizedSpy.count(), 1);
    QCOMPARE(moveResizedChangedSpy.count(), 1);
    QCOMPARE(win::is_resize(c), true);
    QVERIFY(configureRequestedSpy.wait());
    QCOMPARE(configureRequestedSpy.count(), 3);

    cfgdata = shellSurface->get_configure_data();
    QVERIFY(cfgdata.states.testFlag(xdg_shell_state::activated));
    QVERIFY(cfgdata.states.testFlag(xdg_shell_state::resizing));

    // Trigger a change.
    auto const cursorPos = Test::cursor()->pos();
    win::key_press_event(c, Qt::Key_Right);
    win::update_move_resize(c, Test::cursor()->pos());
    QCOMPARE(Test::cursor()->pos(), cursorPos + QPoint(8, 0));

    // The client should receive a configure event with the new size.
    QVERIFY(configureRequestedSpy.wait());
    QCOMPARE(configureRequestedSpy.count(), 4);

    cfgdata = shellSurface->get_configure_data();
    QVERIFY(cfgdata.states.testFlag(xdg_shell_state::activated));
    QVERIFY(cfgdata.states.testFlag(xdg_shell_state::resizing));
    QCOMPARE(cfgdata.size, QSize(108, 50));
    QVERIFY(cfgdata.updates.testFlag(xdg_shell_toplevel_configure_change::size));
    QCOMPARE(clientStepUserMovedResizedSpy.count(), 0);

    // Now render new size.
    shellSurface->ackConfigure(configureRequestedSpy.back().front().value<quint32>());
    Test::render(surface, QSize(108, 50), Qt::blue);
    QVERIFY(geometryChangedSpy.wait());
    QCOMPARE(c->geo.frame, QRect(0, 0, 108, 50));
    QCOMPARE(clientStepUserMovedResizedSpy.count(), 1);

    // Go down.
    win::key_press_event(c, Qt::Key_Down);
    win::update_move_resize(c, Test::cursor()->pos());
    QCOMPARE(Test::cursor()->pos(), cursorPos + QPoint(8, 8));

    // The client should receive another configure event.
    QVERIFY(configureRequestedSpy.wait());
    QCOMPARE(configureRequestedSpy.count(), 5);

    cfgdata = shellSurface->get_configure_data();
    QVERIFY(cfgdata.states.testFlag(xdg_shell_state::activated));
    QVERIFY(cfgdata.states.testFlag(xdg_shell_state::resizing));
    QCOMPARE(cfgdata.size, QSize(108, 58));
    QVERIFY(cfgdata.updates.testFlag(xdg_shell_toplevel_configure_change::size));

    // Now render new size.
    shellSurface->ackConfigure(configureRequestedSpy.back().front().value<quint32>());
    Test::render(surface, QSize(108, 58), Qt::blue);
    QVERIFY(geometryChangedSpy.wait());
    QCOMPARE(c->geo.frame, QRect(0, 0, 108, 58));
    QCOMPARE(clientStepUserMovedResizedSpy.count(), 2);

    // Let's finalize the resize operation.
    QCOMPARE(clientFinishUserMovedResizedSpy.count(), 0);
    win::key_press_event(c, Qt::Key_Enter);
    QCOMPARE(clientFinishUserMovedResizedSpy.count(), 1);
    QCOMPARE(moveResizedChangedSpy.count(), 2);
    QCOMPARE(win::is_resize(c), false);
    QVERIFY(!Test::app()->base->space->move_resize_window);
    QEXPECT_FAIL("", "XdgShellClient currently doesn't send final configure event", Abort);
    QVERIFY(configureRequestedSpy.wait(500));
    QCOMPARE(configureRequestedSpy.count(), 6);

    cfgdata = shellSurface->get_configure_data();
    QVERIFY(cfgdata.states.testFlag(xdg_shell_state::activated));
    QVERIFY(!cfgdata.states.testFlag(xdg_shell_state::resizing));

    // Destroy the client.
    surface.reset();
    QVERIFY(Test::wait_for_destroyed(c));
}

void MoveResizeWindowTest::testPackTo_data()
{
    QTest::addColumn<QRect>("expectedGeometry");

    QTest::newRow("left") << QRect(0, 487, 100, 50);
    QTest::newRow("up") << QRect(590, 0, 100, 50);
    QTest::newRow("right") << QRect(1180, 487, 100, 50);
    QTest::newRow("down") << QRect(590, 974, 100, 50);
}

std::function<void(Test::space&)> get_space_pack_method(std::string const& method_name)
{
    if (method_name == "left") {
        return win::active_window_pack_left<Test::space>;
    } else if (method_name == "up") {
        return &win::active_window_pack_up<Test::space>;
    } else if (method_name == "right") {
        return &win::active_window_pack_right<Test::space>;
    } else if (method_name == "down") {
        return &win::active_window_pack_down<Test::space>;
    }
    return {};
}

void MoveResizeWindowTest::testPackTo()
{
    using namespace Wrapland::Client;

    std::unique_ptr<Surface> surface(Test::create_surface());
    QVERIFY(surface);

    std::unique_ptr<XdgShellToplevel> shellSurface(Test::create_xdg_shell_toplevel(surface));
    QVERIFY(shellSurface);

    // let's render
    auto c = Test::render_and_wait_for_shown(surface, QSize(100, 50), Qt::blue);

    QVERIFY(c);
    QCOMPARE(Test::get_wayland_window(Test::app()->base->space->stacking.active), c);
    QCOMPARE(c->geo.frame, QRect(0, 0, 100, 50));

    // let's place it centered
    win::place_centered(c, QRect(0, 0, 1280, 1024));
    QCOMPARE(c->geo.frame, QRect(590, 487, 100, 50));

    auto method_call = get_space_pack_method(QTest::currentDataTag());
    QVERIFY(method_call);
    method_call(*Test::app()->base->space.get());

    QTEST(c->geo.frame, "expectedGeometry");
    surface.reset();
    QVERIFY(Test::wait_for_destroyed(c));
}

void MoveResizeWindowTest::testPackAgainstClient_data()
{
    QTest::addColumn<QRect>("expectedGeometry");

    QTest::newRow("left") << QRect(10, 487, 100, 50);
    QTest::newRow("up") << QRect(590, 10, 100, 50);
    QTest::newRow("right") << QRect(1170, 487, 100, 50);
    QTest::newRow("down") << QRect(590, 964, 100, 50);
}

void MoveResizeWindowTest::testPackAgainstClient()
{
    using namespace Wrapland::Client;

    std::unique_ptr<Surface> surface1(Test::create_surface());
    QVERIFY(surface1);
    std::unique_ptr<Surface> surface2(Test::create_surface());
    QVERIFY(surface2);
    std::unique_ptr<Surface> surface3(Test::create_surface());
    QVERIFY(surface3);
    std::unique_ptr<Surface> surface4(Test::create_surface());
    QVERIFY(surface4);

    std::unique_ptr<XdgShellToplevel> shellSurface1(Test::create_xdg_shell_toplevel(surface1));
    QVERIFY(shellSurface1);
    std::unique_ptr<XdgShellToplevel> shellSurface2(Test::create_xdg_shell_toplevel(surface2));
    QVERIFY(shellSurface2);
    std::unique_ptr<XdgShellToplevel> shellSurface3(Test::create_xdg_shell_toplevel(surface3));
    QVERIFY(shellSurface3);
    std::unique_ptr<XdgShellToplevel> shellSurface4(Test::create_xdg_shell_toplevel(surface4));
    QVERIFY(shellSurface4);
    auto renderWindow = [](std::unique_ptr<Surface> const& surface,
                           std::function<void(Test::space&)> const& method_call,
                           const QRect& expectedGeometry) {
        // let's render
        auto c = Test::render_and_wait_for_shown(surface, QSize(10, 10), Qt::blue);

        QVERIFY(c);
        QCOMPARE(Test::get_wayland_window(Test::app()->base->space->stacking.active), c);
        QCOMPARE(c->geo.frame.size(), QSize(10, 10));
        // let's place it centered
        win::place_centered(c, QRect(0, 0, 1280, 1024));
        QCOMPARE(c->geo.frame, QRect(635, 507, 10, 10));
        method_call(*Test::app()->base->space.get());
        QCOMPARE(c->geo.frame, expectedGeometry);
    };
    renderWindow(surface1, &win::active_window_pack_left<Test::space>, QRect(0, 507, 10, 10));
    renderWindow(surface2, &win::active_window_pack_up<Test::space>, QRect(635, 0, 10, 10));
    renderWindow(surface3, &win::active_window_pack_right<Test::space>, QRect(1270, 507, 10, 10));
    renderWindow(surface4, &win::active_window_pack_down<Test::space>, QRect(635, 1014, 10, 10));

    std::unique_ptr<Surface> surface(Test::create_surface());
    QVERIFY(surface);
    std::unique_ptr<XdgShellToplevel> shellSurface(Test::create_xdg_shell_toplevel(surface));
    QVERIFY(shellSurface);
    auto c = Test::render_and_wait_for_shown(surface, QSize(100, 50), Qt::blue);

    QVERIFY(c);
    QCOMPARE(Test::get_wayland_window(Test::app()->base->space->stacking.active), c);
    // let's place it centered
    win::place_centered(c, QRect(0, 0, 1280, 1024));
    QCOMPARE(c->geo.frame, QRect(590, 487, 100, 50));

    auto method_call = get_space_pack_method(QTest::currentDataTag());
    QVERIFY(method_call);
    method_call(*Test::app()->base->space.get());
    QTEST(c->geo.frame, "expectedGeometry");
}

std::function<void(Test::space&)> get_space_grow_shrink_method(std::string const& method_name)
{
    if (method_name == "grow vertical") {
        return win::active_window_grow_vertical<Test::space>;
    } else if (method_name == "grow horizontal") {
        return win::active_window_grow_horizontal<Test::space>;
    } else if (method_name == "shrink vertical") {
        return win::active_window_shrink_vertical<Test::space>;
    } else if (method_name == "shrink horizontal") {
        return win::active_window_shrink_horizontal<Test::space>;
    }
    return {};
}

void MoveResizeWindowTest::testGrowShrink_data()
{
    QTest::addColumn<QRect>("expectedGeometry");

    QTest::newRow("grow vertical") << QRect(590, 487, 100, 537);
    QTest::newRow("grow horizontal") << QRect(590, 487, 690, 50);
    QTest::newRow("shrink vertical") << QRect(590, 487, 100, 23);
    QTest::newRow("shrink horizontal") << QRect(590, 487, 40, 50);
}

void MoveResizeWindowTest::testGrowShrink()
{
    using namespace Wrapland::Client;

    // This helper surface ensures the test surface will shrink when calling the respective methods.
    std::unique_ptr<Surface> surface1(Test::create_surface());
    QVERIFY(surface1);
    std::unique_ptr<XdgShellToplevel> shellSurface1(Test::create_xdg_shell_toplevel(surface1));
    QVERIFY(shellSurface1);
    auto window = Test::render_and_wait_for_shown(surface1, QSize(650, 514), Qt::blue);
    QVERIFY(window);
    win::active_window_pack_right(*Test::app()->base->space);
    win::active_window_pack_down(*Test::app()->base->space);

    std::unique_ptr<Surface> surface(Test::create_surface());
    QVERIFY(surface);

    std::unique_ptr<XdgShellToplevel> shellSurface(Test::create_xdg_shell_toplevel(surface));
    QVERIFY(shellSurface);

    QSignalSpy configure_spy(shellSurface.get(), &XdgShellToplevel::configured);
    QVERIFY(configure_spy.isValid());

    auto c = Test::render_and_wait_for_shown(surface, QSize(100, 50), Qt::blue);
    QVERIFY(c);
    QCOMPARE(Test::get_wayland_window(Test::app()->base->space->stacking.active), c);

    // Configure event due to activation.
    QVERIFY(configure_spy.wait());
    QCOMPARE(configure_spy.count(), 1);

    QSignalSpy geometryChangedSpy(c->qobject.get(), &win::window_qobject::frame_geometry_changed);
    QVERIFY(geometryChangedSpy.isValid());

    win::place_centered(c, QRect(0, 0, 1280, 1024));
    QCOMPARE(c->geo.frame, QRect(590, 487, 100, 50));

    // Now according to test data grow/shrink vertically/horizontally.
    auto method_call = get_space_grow_shrink_method(QTest::currentDataTag());
    QVERIFY(method_call);
    method_call(*Test::app()->base->space.get());

    QVERIFY(configure_spy.wait());
    QCOMPARE(configure_spy.count(), 2);

    shellSurface->ackConfigure(configure_spy.back().front().value<quint32>());
    Test::render(surface, shellSurface->get_configure_data().size, Qt::red);

    QVERIFY(geometryChangedSpy.wait());
    QTEST(c->geo.frame, "expectedGeometry");
}

void MoveResizeWindowTest::testPointerMoveEnd_data()
{
    QTest::addColumn<int>("additionalButton");

    QTest::newRow("BTN_RIGHT") << BTN_RIGHT;
    QTest::newRow("BTN_MIDDLE") << BTN_MIDDLE;
    QTest::newRow("BTN_SIDE") << BTN_SIDE;
    QTest::newRow("BTN_EXTRA") << BTN_EXTRA;
    QTest::newRow("BTN_FORWARD") << BTN_FORWARD;
    QTest::newRow("BTN_BACK") << BTN_BACK;
    QTest::newRow("BTN_TASK") << BTN_TASK;
    for (int i = BTN_TASK + 1; i < BTN_JOYSTICK; i++) {
        QTest::newRow(QByteArray::number(i, 16).constData()) << i;
    }
}

void MoveResizeWindowTest::testPointerMoveEnd()
{
    // this test verifies that moving a window through pointer only ends if all buttons are released
    using namespace Wrapland::Client;

    std::unique_ptr<Surface> surface(Test::create_surface());
    QVERIFY(surface);

    std::unique_ptr<XdgShellToplevel> shellSurface(Test::create_xdg_shell_toplevel(surface));
    QVERIFY(shellSurface);

    // let's render
    auto c = Test::render_and_wait_for_shown(surface, QSize(100, 50), Qt::blue);

    QVERIFY(c);
    QCOMPARE(c, Test::get_wayland_window(Test::app()->base->space->stacking.active));
    QVERIFY(!win::is_move(c));

    // let's trigger the left button
    quint32 timestamp = 1;
    Test::pointer_button_pressed(BTN_LEFT, timestamp++);
    QVERIFY(!win::is_move(c));
    win::active_window_move(*Test::app()->base->space);
    QVERIFY(win::is_move(c));

    // let's press another button
    QFETCH(int, additionalButton);
    Test::pointer_button_pressed(additionalButton, timestamp++);
    QVERIFY(win::is_move(c));

    // release the left button, should still have the window moving
    Test::pointer_button_released(BTN_LEFT, timestamp++);
    QVERIFY(win::is_move(c));

    // but releasing the other button should now end moving
    Test::pointer_button_released(additionalButton, timestamp++);
    QVERIFY(!win::is_move(c));
    surface.reset();
    QVERIFY(Test::wait_for_destroyed(c));
}

void MoveResizeWindowTest::testClientSideMove()
{
    using namespace Wrapland::Client;
    Test::cursor()->set_pos(640, 512);
    std::unique_ptr<Pointer> pointer(Test::get_client().interfaces.seat->createPointer());
    QSignalSpy pointerEnteredSpy(pointer.get(), &Pointer::entered);
    QVERIFY(pointerEnteredSpy.isValid());
    QSignalSpy pointerLeftSpy(pointer.get(), &Pointer::left);
    QVERIFY(pointerLeftSpy.isValid());
    QSignalSpy buttonSpy(pointer.get(), &Pointer::buttonStateChanged);
    QVERIFY(buttonSpy.isValid());

    std::unique_ptr<Surface> surface(Test::create_surface());
    std::unique_ptr<XdgShellToplevel> shellSurface(Test::create_xdg_shell_toplevel(surface));
    auto c = Test::render_and_wait_for_shown(surface, QSize(100, 50), Qt::blue);
    QVERIFY(c);

    // move pointer into center of geometry
    const QRect startGeometry = c->geo.frame;
    Test::cursor()->set_pos(startGeometry.center());
    QVERIFY(pointerEnteredSpy.wait());
    QCOMPARE(pointerEnteredSpy.first().last().toPoint(), QPoint(49, 24));
    // simulate press
    quint32 timestamp = 1;
    Test::pointer_button_pressed(BTN_LEFT, timestamp++);
    QVERIFY(buttonSpy.wait());
    QSignalSpy moveStartSpy(c->qobject.get(), &win::window_qobject::clientStartUserMovedResized);
    QVERIFY(moveStartSpy.isValid());
    shellSurface->requestMove(Test::get_client().interfaces.seat.get(),
                              buttonSpy.first().first().value<quint32>());
    QVERIFY(moveStartSpy.wait());
    QCOMPARE(win::is_move(c), true);
    QVERIFY(pointerLeftSpy.wait());

    // move a bit
    QSignalSpy clientMoveStepSpy(c->qobject.get(),
                                 &win::window_qobject::clientStepUserMovedResized);
    QVERIFY(clientMoveStepSpy.isValid());
    const QPoint startPoint = startGeometry.center();
    const int dragDistance = QApplication::startDragDistance();
    // Why?
    Test::pointer_motion_absolute(startPoint + QPoint(dragDistance, dragDistance) + QPoint(6, 6),
                                  timestamp++);
    QCOMPARE(clientMoveStepSpy.count(), 1);

    // and release again
    Test::pointer_button_released(BTN_LEFT, timestamp++);
    QVERIFY(pointerEnteredSpy.wait());
    QCOMPARE(win::is_move(c), false);
    QCOMPARE(c->geo.frame,
             startGeometry.translated(QPoint(dragDistance, dragDistance) + QPoint(6, 6)));
    QCOMPARE(pointerEnteredSpy.last().last().toPoint(), QPoint(49, 24));
}

void MoveResizeWindowTest::testPlasmaShellSurfaceMovable_data()
{
    QTest::addColumn<Wrapland::Client::PlasmaShellSurface::Role>("role");
    QTest::addColumn<bool>("movable");
    QTest::addColumn<bool>("movableAcrossScreens");
    QTest::addColumn<bool>("resizable");

    QTest::newRow("normal") << Wrapland::Client::PlasmaShellSurface::Role::Normal << true << true
                            << true;
    QTest::newRow("desktop") << Wrapland::Client::PlasmaShellSurface::Role::Desktop << false
                             << false << false;
    QTest::newRow("panel") << Wrapland::Client::PlasmaShellSurface::Role::Panel << false << false
                           << false;
    QTest::newRow("osd") << Wrapland::Client::PlasmaShellSurface::Role::OnScreenDisplay << false
                         << false << false;
}

void MoveResizeWindowTest::testPlasmaShellSurfaceMovable()
{
    // this test verifies that certain window types from PlasmaShellSurface are not moveable or
    // resizable
    using namespace Wrapland::Client;
    std::unique_ptr<Surface> surface(Test::create_surface());
    QVERIFY(surface);

    std::unique_ptr<XdgShellToplevel> shellSurface(Test::create_xdg_shell_toplevel(surface));
    QVERIFY(shellSurface);
    // and a PlasmaShellSurface
    std::unique_ptr<PlasmaShellSurface> plasmaSurface(
        Test::get_client().interfaces.plasma_shell->createSurface(surface.get()));
    QVERIFY(plasmaSurface);
    QFETCH(Wrapland::Client::PlasmaShellSurface::Role, role);
    plasmaSurface->setRole(role);
    // let's render
    auto c = Test::render_and_wait_for_shown(surface, QSize(100, 50), Qt::blue);

    QVERIFY(c);
    QTEST(c->isMovable(), "movable");
    QTEST(c->isMovableAcrossScreens(), "movableAcrossScreens");
    QTEST(c->isResizable(), "resizable");
    surface.reset();
    QVERIFY(Test::wait_for_destroyed(c));
}

void xcb_connection_deleter(xcb_connection_t* pointer)
{
    xcb_disconnect(pointer);
}

using xcb_connection_ptr = std::unique_ptr<xcb_connection_t, void (*)(xcb_connection_t*)>;

xcb_connection_ptr create_xcb_connection()
{
    return xcb_connection_ptr(xcb_connect(nullptr, nullptr), xcb_connection_deleter);
}

void MoveResizeWindowTest::testNetMove()
{
    // this test verifies that a move request for an X11 window through NET API works
    // create an xcb window
    auto c = create_xcb_connection();
    QVERIFY(!xcb_connection_has_error(c.get()));

    xcb_window_t w = xcb_generate_id(c.get());
    xcb_create_window(c.get(),
                      XCB_COPY_FROM_PARENT,
                      w,
                      Test::app()->base->x11_data.root_window,
                      0,
                      0,
                      100,
                      100,
                      0,
                      XCB_WINDOW_CLASS_INPUT_OUTPUT,
                      XCB_COPY_FROM_PARENT,
                      0,
                      nullptr);
    xcb_size_hints_t hints;
    memset(&hints, 0, sizeof(hints));
    xcb_icccm_size_hints_set_position(&hints, 1, 0, 0);
    xcb_icccm_size_hints_set_size(&hints, 1, 100, 100);
    xcb_icccm_set_wm_normal_hints(c.get(), w, &hints);
    // let's set a no-border
    NETWinInfo winInfo(
        c.get(), w, Test::app()->base->x11_data.root_window, NET::WMWindowType, NET::Properties2());
    winInfo.setWindowType(NET::Override);
    xcb_map_window(c.get(), w);
    xcb_flush(c.get());

    QSignalSpy windowCreatedSpy(Test::app()->base->space->qobject.get(),
                                &win::space_qobject::clientAdded);
    QVERIFY(windowCreatedSpy.isValid());
    QVERIFY(windowCreatedSpy.wait());

    auto client = get_x11_window_from_id(windowCreatedSpy.first().first().value<quint32>());
    QVERIFY(client);
    QCOMPARE(client->xcb_windows.client, w);
    const QRect origGeo = client->geo.frame;

    // let's move the cursor outside the window
    Test::cursor()->set_pos(Test::get_output(0)->geometry().center());
    QVERIFY(!origGeo.contains(Test::cursor()->pos()));

    QSignalSpy moveStartSpy(client->qobject.get(),
                            &win::window_qobject::clientStartUserMovedResized);
    QVERIFY(moveStartSpy.isValid());
    QSignalSpy moveEndSpy(client->qobject.get(),
                          &win::window_qobject::clientFinishUserMovedResized);
    QVERIFY(moveEndSpy.isValid());
    QSignalSpy moveStepSpy(client->qobject.get(), &win::window_qobject::clientStepUserMovedResized);
    QVERIFY(moveStepSpy.isValid());
    QVERIFY(!Test::app()->base->space->move_resize_window);

    // use NETRootInfo to trigger a move request
    NETRootInfo root(c.get(), NET::Properties());
    root.moveResizeRequest(w, origGeo.center().x(), origGeo.center().y(), NET::Move);
    xcb_flush(c.get());

    QVERIFY(moveStartSpy.wait());
    QCOMPARE(Test::get_x11_window(Test::app()->base->space->move_resize_window), client);
    QVERIFY(win::is_move(client));
    QCOMPARE(client->geo.restore.max, origGeo);
    QCOMPARE(Test::cursor()->pos(), origGeo.center());

    // let's move a step
    Test::cursor()->set_pos(Test::cursor()->pos() + QPoint(10, 10));
    QCOMPARE(moveStepSpy.count(), 1);
    QCOMPARE(moveStepSpy.first().last().toRect(), origGeo.translated(10, 10));

    // let's cancel the move resize again through the net API
    root.moveResizeRequest(
        w, client->geo.frame.center().x(), client->geo.frame.center().y(), NET::MoveResizeCancel);
    xcb_flush(c.get());
    QVERIFY(moveEndSpy.wait());

    // and destroy the window again
    xcb_unmap_window(c.get(), w);
    xcb_destroy_window(c.get(), w);
    xcb_flush(c.get());
    c.reset();

    QSignalSpy windowClosedSpy(client->qobject.get(), &win::window_qobject::closed);
    QVERIFY(windowClosedSpy.isValid());
    QVERIFY(windowClosedSpy.wait());
}

void MoveResizeWindowTest::testAdjustClientGeometryOfAutohidingX11Panel_data()
{
    QTest::addColumn<QRect>("panelGeometry");
    QTest::addColumn<QPoint>("targetPoint");
    QTest::addColumn<QPoint>("expectedAdjustedPoint");
    QTest::addColumn<quint32>("hideLocation");

    QTest::newRow("top") << QRect(0, 0, 100, 20) << QPoint(50, 25) << QPoint(50, 20) << 0u;
    QTest::newRow("bottom") << QRect(0, 1024 - 20, 100, 20) << QPoint(50, 1024 - 25 - 50)
                            << QPoint(50, 1024 - 20 - 50) << 2u;
    QTest::newRow("left") << QRect(0, 0, 20, 100) << QPoint(25, 50) << QPoint(20, 50) << 3u;
    QTest::newRow("right") << QRect(1280 - 20, 0, 20, 100) << QPoint(1280 - 25 - 100, 50)
                           << QPoint(1280 - 20 - 100, 50) << 1u;
}

void MoveResizeWindowTest::testAdjustClientGeometryOfAutohidingX11Panel()
{
    // this test verifies that auto hiding panels are ignored when adjusting client geometry
    // see BUG 365892

    // first create our panel
    auto c = create_xcb_connection();
    QVERIFY(!xcb_connection_has_error(c.get()));

    xcb_window_t w = xcb_generate_id(c.get());
    QFETCH(QRect, panelGeometry);
    xcb_create_window(c.get(),
                      XCB_COPY_FROM_PARENT,
                      w,
                      Test::app()->base->x11_data.root_window,
                      panelGeometry.x(),
                      panelGeometry.y(),
                      panelGeometry.width(),
                      panelGeometry.height(),
                      0,
                      XCB_WINDOW_CLASS_INPUT_OUTPUT,
                      XCB_COPY_FROM_PARENT,
                      0,
                      nullptr);
    xcb_size_hints_t hints;
    memset(&hints, 0, sizeof(hints));
    xcb_icccm_size_hints_set_position(&hints, 1, panelGeometry.x(), panelGeometry.y());
    xcb_icccm_size_hints_set_size(&hints, 1, panelGeometry.width(), panelGeometry.height());
    xcb_icccm_set_wm_normal_hints(c.get(), w, &hints);
    NETWinInfo winInfo(
        c.get(), w, Test::app()->base->x11_data.root_window, NET::WMWindowType, NET::Properties2());
    winInfo.setWindowType(NET::Dock);
    xcb_map_window(c.get(), w);
    xcb_flush(c.get());

    QSignalSpy windowCreatedSpy(Test::app()->base->space->qobject.get(),
                                &win::space_qobject::clientAdded);
    QVERIFY(windowCreatedSpy.isValid());
    QVERIFY(windowCreatedSpy.wait());

    auto panel = get_x11_window_from_id(windowCreatedSpy.first().first().value<quint32>());
    QVERIFY(panel);
    QCOMPARE(panel->xcb_windows.client, w);
    QCOMPARE(panel->geo.frame, panelGeometry);
    QVERIFY(win::is_dock(panel));

    // let's create a window
    using namespace Wrapland::Client;
    std::unique_ptr<Surface> surface(Test::create_surface());
    QVERIFY(surface);

    std::unique_ptr<XdgShellToplevel> shellSurface(Test::create_xdg_shell_toplevel(surface));
    QVERIFY(shellSurface);
    auto testWindow = Test::render_and_wait_for_shown(surface, QSize(100, 50), Qt::blue);

    QVERIFY(testWindow);
    QVERIFY(testWindow->isMovable());
    // panel is not yet hidden, we should snap against it
    QFETCH(QPoint, targetPoint);
    QTEST(win::adjust_window_position(*Test::app()->base->space, *testWindow, targetPoint, false),
          "expectedAdjustedPoint");

    // now let's hide the panel
    QSignalSpy panelHiddenSpy(panel->qobject.get(), &win::window_qobject::windowHidden);
    QVERIFY(panelHiddenSpy.isValid());
    QFETCH(quint32, hideLocation);
    xcb_change_property(c.get(),
                        XCB_PROP_MODE_REPLACE,
                        w,
                        Test::app()->base->space->atoms->kde_screen_edge_show,
                        XCB_ATOM_CARDINAL,
                        32,
                        1,
                        &hideLocation);
    xcb_flush(c.get());
    QVERIFY(panelHiddenSpy.wait());

    // now try to snap again
    QCOMPARE(
        win::adjust_window_position(*Test::app()->base->space, *testWindow, targetPoint, false),
        targetPoint);

    // and destroy the panel again
    xcb_unmap_window(c.get(), w);
    xcb_destroy_window(c.get(), w);
    xcb_flush(c.get());
    c.reset();

    QSignalSpy panelClosedSpy(panel->qobject.get(), &win::window_qobject::closed);
    QVERIFY(panelClosedSpy.isValid());
    QVERIFY(panelClosedSpy.wait());

    // snap once more
    QCOMPARE(
        win::adjust_window_position(*Test::app()->base->space, *testWindow, targetPoint, false),
        targetPoint);

    // and close
    QSignalSpy windowClosedSpy(testWindow->qobject.get(), &win::window_qobject::closed);
    QVERIFY(windowClosedSpy.isValid());
    shellSurface.reset();
    surface.reset();
    QVERIFY(windowClosedSpy.wait());
}

void MoveResizeWindowTest::testAdjustClientGeometryOfAutohidingWaylandPanel_data()
{
    QTest::addColumn<QRect>("panelGeometry");
    QTest::addColumn<QPoint>("targetPoint");
    QTest::addColumn<QPoint>("expectedAdjustedPoint");

    QTest::newRow("top") << QRect(0, 0, 100, 20) << QPoint(50, 25) << QPoint(50, 20);
    QTest::newRow("bottom") << QRect(0, 1024 - 20, 100, 20) << QPoint(50, 1024 - 25 - 50)
                            << QPoint(50, 1024 - 20 - 50);
    QTest::newRow("left") << QRect(0, 0, 20, 100) << QPoint(25, 50) << QPoint(20, 50);
    QTest::newRow("right") << QRect(1280 - 20, 0, 20, 100) << QPoint(1280 - 25 - 100, 50)
                           << QPoint(1280 - 20 - 100, 50);
}

void MoveResizeWindowTest::testAdjustClientGeometryOfAutohidingWaylandPanel()
{
    // this test verifies that auto hiding panels are ignored when adjusting client geometry
    // see BUG 365892

    // first create our panel
    using namespace Wrapland::Client;
    std::unique_ptr<Surface> panelSurface(Test::create_surface());
    QVERIFY(panelSurface);
    std::unique_ptr<XdgShellToplevel> panelShellSurface(
        Test::create_xdg_shell_toplevel(panelSurface));
    QVERIFY(panelShellSurface);
    std::unique_ptr<PlasmaShellSurface> plasmaSurface(
        Test::get_client().interfaces.plasma_shell->createSurface(panelSurface.get()));
    QVERIFY(plasmaSurface);
    plasmaSurface->setRole(PlasmaShellSurface::Role::Panel);
    plasmaSurface->setPanelBehavior(PlasmaShellSurface::PanelBehavior::AutoHide);
    QFETCH(QRect, panelGeometry);
    plasmaSurface->setPosition(panelGeometry.topLeft());
    // let's render
    auto panel = Test::render_and_wait_for_shown(panelSurface, panelGeometry.size(), Qt::blue);
    QVERIFY(panel);
    QCOMPARE(panel->geo.frame, panelGeometry);
    QVERIFY(win::is_dock(panel));

    // let's create a window
    std::unique_ptr<Surface> surface(Test::create_surface());
    QVERIFY(surface);

    std::unique_ptr<XdgShellToplevel> shellSurface(Test::create_xdg_shell_toplevel(surface));
    QVERIFY(shellSurface);
    auto testWindow = Test::render_and_wait_for_shown(surface, QSize(100, 50), Qt::blue);

    QVERIFY(testWindow);
    QVERIFY(testWindow->isMovable());
    // panel is not yet hidden, we should snap against it
    QFETCH(QPoint, targetPoint);
    QTEST(win::adjust_window_position(*Test::app()->base->space, *testWindow, targetPoint, false),
          "expectedAdjustedPoint");

    // now let's hide the panel
    QSignalSpy panelHiddenSpy(panel->qobject.get(), &win::window_qobject::windowHidden);
    QVERIFY(panelHiddenSpy.isValid());
    plasmaSurface->requestHideAutoHidingPanel();
    QVERIFY(panelHiddenSpy.wait());

    // now try to snap again
    QCOMPARE(
        win::adjust_window_position(*Test::app()->base->space, *testWindow, targetPoint, false),
        targetPoint);

    // and destroy the panel again
    QSignalSpy panelClosedSpy(panel->qobject.get(), &win::window_qobject::closed);
    QVERIFY(panelClosedSpy.isValid());
    plasmaSurface.reset();
    panelShellSurface.reset();
    panelSurface.reset();
    QVERIFY(panelClosedSpy.wait());

    // snap once more
    QCOMPARE(
        win::adjust_window_position(*Test::app()->base->space, *testWindow, targetPoint, false),
        targetPoint);

    // and close
    QSignalSpy windowClosedSpy(testWindow->qobject.get(), &win::window_qobject::closed);
    QVERIFY(windowClosedSpy.isValid());
    shellSurface.reset();
    surface.reset();
    QVERIFY(windowClosedSpy.wait());
}

void MoveResizeWindowTest::testDestroyMoveClient()
{
    // This test verifies that active move operation gets finished when
    // the associated client is destroyed.

    // Create the test client.
    using namespace Wrapland::Client;
    std::unique_ptr<Surface> surface(Test::create_surface());
    QVERIFY(surface);
    std::unique_ptr<XdgShellToplevel> shellSurface(Test::create_xdg_shell_toplevel(surface));
    QVERIFY(shellSurface);
    auto client = Test::render_and_wait_for_shown(surface, QSize(100, 50), Qt::blue);
    QVERIFY(client);

    // Start moving the client.
    QSignalSpy clientStartMoveResizedSpy(client->qobject.get(),
                                         &win::window_qobject::clientStartUserMovedResized);
    QVERIFY(clientStartMoveResizedSpy.isValid());
    QSignalSpy clientFinishUserMovedResizedSpy(client->qobject.get(),
                                               &win::window_qobject::clientFinishUserMovedResized);
    QVERIFY(clientFinishUserMovedResizedSpy.isValid());

    QVERIFY(!Test::app()->base->space->move_resize_window);
    QCOMPARE(win::is_move(client), false);
    QCOMPARE(win::is_resize(client), false);
    win::active_window_move(*Test::app()->base->space);
    QCOMPARE(clientStartMoveResizedSpy.count(), 1);
    QCOMPARE(Test::get_wayland_window(Test::app()->base->space->move_resize_window), client);
    QCOMPARE(win::is_move(client), true);
    QCOMPARE(win::is_resize(client), false);

    // Let's pretend that the client crashed.
    shellSurface.reset();
    surface.reset();
    QVERIFY(Test::wait_for_destroyed(client));
    QCOMPARE(clientFinishUserMovedResizedSpy.count(), 0);
    QVERIFY(!Test::app()->base->space->move_resize_window);
}

void MoveResizeWindowTest::testDestroyResizeClient()
{
    // This test verifies that active resize operation gets finished when
    // the associated client is destroyed.

    // Create the test client.
    using namespace Wrapland::Client;
    std::unique_ptr<Surface> surface(Test::create_surface());
    QVERIFY(surface);
    std::unique_ptr<XdgShellToplevel> shellSurface(Test::create_xdg_shell_toplevel(surface));
    QVERIFY(shellSurface);
    auto client = Test::render_and_wait_for_shown(surface, QSize(100, 50), Qt::blue);
    QVERIFY(client);

    // Start resizing the client.
    QSignalSpy clientStartMoveResizedSpy(client->qobject.get(),
                                         &win::window_qobject::clientStartUserMovedResized);
    QVERIFY(clientStartMoveResizedSpy.isValid());
    QSignalSpy clientFinishUserMovedResizedSpy(client->qobject.get(),
                                               &win::window_qobject::clientFinishUserMovedResized);
    QVERIFY(clientFinishUserMovedResizedSpy.isValid());

    QVERIFY(!Test::app()->base->space->move_resize_window);
    QCOMPARE(win::is_move(client), false);
    QCOMPARE(win::is_resize(client), false);
    win::active_window_resize(*Test::app()->base->space);
    QCOMPARE(clientStartMoveResizedSpy.count(), 1);
    QCOMPARE(Test::get_wayland_window(Test::app()->base->space->move_resize_window), client);
    QCOMPARE(win::is_move(client), false);
    QCOMPARE(win::is_resize(client), true);

    // Let's pretend that the client crashed.
    shellSurface.reset();
    surface.reset();
    QVERIFY(Test::wait_for_destroyed(client));
    QCOMPARE(clientFinishUserMovedResizedSpy.count(), 0);
    QVERIFY(!Test::app()->base->space->move_resize_window);
}

void MoveResizeWindowTest::testUnmapMoveClient()
{
    // This test verifies that active move operation gets cancelled when
    // the associated client is unmapped.

    // Create the test client.
    using namespace Wrapland::Client;
    std::unique_ptr<Surface> surface(Test::create_surface());
    QVERIFY(surface);
    std::unique_ptr<XdgShellToplevel> shellSurface(Test::create_xdg_shell_toplevel(surface));
    QVERIFY(shellSurface);
    auto client = Test::render_and_wait_for_shown(surface, QSize(100, 50), Qt::blue);
    QVERIFY(client);

    // Start resizing the client.
    QSignalSpy clientStartMoveResizedSpy(client->qobject.get(),
                                         &win::window_qobject::clientStartUserMovedResized);
    QVERIFY(clientStartMoveResizedSpy.isValid());
    QSignalSpy clientFinishUserMovedResizedSpy(client->qobject.get(),
                                               &win::window_qobject::clientFinishUserMovedResized);
    QVERIFY(clientFinishUserMovedResizedSpy.isValid());

    QVERIFY(!Test::app()->base->space->move_resize_window);
    QCOMPARE(win::is_move(client), false);
    QCOMPARE(win::is_resize(client), false);
    win::active_window_move(*Test::app()->base->space);
    QCOMPARE(clientStartMoveResizedSpy.count(), 1);
    QCOMPARE(Test::get_wayland_window(Test::app()->base->space->move_resize_window), client);
    QCOMPARE(win::is_move(client), true);
    QCOMPARE(win::is_resize(client), false);

    // Unmap the client while we're moving it.
    QSignalSpy hiddenSpy(client->qobject.get(), &win::window_qobject::windowHidden);
    QVERIFY(hiddenSpy.isValid());
    surface->attachBuffer(Buffer::Ptr());
    surface->commit(Surface::CommitFlag::None);
    QVERIFY(hiddenSpy.wait());
    QCOMPARE(clientFinishUserMovedResizedSpy.count(), 0);
    QVERIFY(!Test::app()->base->space->move_resize_window);
    QCOMPARE(win::is_move(client), false);
    QCOMPARE(win::is_resize(client), false);

    // Destroy the client.
    shellSurface.reset();
    QVERIFY(Test::wait_for_destroyed(client));
    QCOMPARE(clientFinishUserMovedResizedSpy.count(), 0);
}

void MoveResizeWindowTest::testUnmapResizeClient()
{
    // This test verifies that active resize operation gets cancelled when
    // the associated client is unmapped.

    // Create the test client.
    using namespace Wrapland::Client;
    std::unique_ptr<Surface> surface(Test::create_surface());
    QVERIFY(surface);
    std::unique_ptr<XdgShellToplevel> shellSurface(Test::create_xdg_shell_toplevel(surface));
    QVERIFY(shellSurface);
    auto client = Test::render_and_wait_for_shown(surface, QSize(100, 50), Qt::blue);
    QVERIFY(client);

    // Start resizing the client.
    QSignalSpy clientStartMoveResizedSpy(client->qobject.get(),
                                         &win::window_qobject::clientStartUserMovedResized);
    QVERIFY(clientStartMoveResizedSpy.isValid());
    QSignalSpy clientFinishUserMovedResizedSpy(client->qobject.get(),
                                               &win::window_qobject::clientFinishUserMovedResized);
    QVERIFY(clientFinishUserMovedResizedSpy.isValid());

    QVERIFY(!Test::app()->base->space->move_resize_window);
    QCOMPARE(win::is_move(client), false);
    QCOMPARE(win::is_resize(client), false);
    win::active_window_resize(*Test::app()->base->space);
    QCOMPARE(clientStartMoveResizedSpy.count(), 1);
    QCOMPARE(Test::get_wayland_window(Test::app()->base->space->move_resize_window), client);
    QCOMPARE(win::is_move(client), false);
    QCOMPARE(win::is_resize(client), true);

    // Unmap the client while we're resizing it.
    QSignalSpy hiddenSpy(client->qobject.get(), &win::window_qobject::windowHidden);
    QVERIFY(hiddenSpy.isValid());
    surface->attachBuffer(Buffer::Ptr());
    surface->commit(Surface::CommitFlag::None);
    QVERIFY(hiddenSpy.wait());
    QCOMPARE(clientFinishUserMovedResizedSpy.count(), 0);
    QVERIFY(!Test::app()->base->space->move_resize_window);
    QCOMPARE(win::is_move(client), false);
    QCOMPARE(win::is_resize(client), false);

    // Destroy the client.
    shellSurface.reset();
    QVERIFY(Test::wait_for_destroyed(client));
    QCOMPARE(clientFinishUserMovedResizedSpy.count(), 0);
}

void MoveResizeWindowTest::testSetFullScreenWhenMoving()
{
    // Ensure we disable moving event when setFullScreen is triggered
    using namespace Wrapland::Client;

    std::unique_ptr<Surface> surface(Test::create_surface());
    QVERIFY(surface);

    std::unique_ptr<XdgShellToplevel> shellSurface(Test::create_xdg_shell_toplevel(surface));
    QVERIFY(shellSurface);

    auto client = Test::render_and_wait_for_shown(surface, QSize(500, 800), Qt::blue);
    QVERIFY(client);

    QSignalSpy fullscreen_spy(client->qobject.get(), &win::window_qobject::fullScreenChanged);
    QVERIFY(fullscreen_spy.isValid());
    QSignalSpy configureRequestedSpy(shellSurface.get(), &XdgShellToplevel::configured);
    QVERIFY(configureRequestedSpy.isValid());
    QVERIFY(configureRequestedSpy.wait());

    win::active_window_move(*Test::app()->base->space);
    QCOMPARE(win::is_move(client), true);

    QVERIFY(configureRequestedSpy.wait());
    QCOMPARE(configureRequestedSpy.count(), 2);

    auto cfgdata = shellSurface->get_configure_data();
    QVERIFY(cfgdata.states.testFlag(xdg_shell_state::activated));
    QVERIFY(!cfgdata.states.testFlag(xdg_shell_state::fullscreen));

    QCOMPARE(cfgdata.size, QSize(500, 800));

    client->setFullScreen(true);

    QCOMPARE(client->control->fullscreen, false);

    QVERIFY(configureRequestedSpy.wait());
    QCOMPARE(configureRequestedSpy.count(), 3);

    cfgdata = shellSurface->get_configure_data();
    QVERIFY(cfgdata.states.testFlag(xdg_shell_state::fullscreen));
    QCOMPARE(cfgdata.size, Test::get_output(0)->geometry().size());

    shellSurface->ackConfigure(configureRequestedSpy.back().front().value<quint32>());
    Test::render(surface, cfgdata.size, Qt::red);

    QVERIFY(fullscreen_spy.wait());
    QCOMPARE(fullscreen_spy.size(), 1);

    QCOMPARE(client->control->fullscreen, true);
    QCOMPARE(win::is_move(client), false);
    QVERIFY(!Test::app()->base->space->move_resize_window);

    // Let's pretend that the client crashed.
    shellSurface.reset();
    surface.reset();
    QVERIFY(Test::wait_for_destroyed(client));
}

void MoveResizeWindowTest::testSetMaximizeWhenMoving()
{
    // Ensure we disable moving event when changeMaximize is triggered
    using namespace Wrapland::Client;

    std::unique_ptr<Surface> surface(Test::create_surface());
    QVERIFY(surface);

    std::unique_ptr<XdgShellToplevel> shellSurface(Test::create_xdg_shell_toplevel(surface));
    QVERIFY(shellSurface);

    // let's render
    auto client = Test::render_and_wait_for_shown(surface, QSize(500, 800), Qt::blue);
    QVERIFY(client);

    win::active_window_move(*Test::app()->base->space);
    QCOMPARE(win::is_move(client), true);
    win::set_maximize(client, true, true);

    QEXPECT_FAIL("", "The client is still in move state at this point. Is this correct?", Abort);
    QCOMPARE(win::is_move(client), false);
    QVERIFY(!Test::app()->base->space->move_resize_window);
    // Let's pretend that the client crashed.
    shellSurface.reset();
    surface.reset();
    QVERIFY(Test::wait_for_destroyed(client));
}

}

WAYLANDTEST_MAIN(KWin::MoveResizeWindowTest)

#include "move_resize_window_test.moc"
